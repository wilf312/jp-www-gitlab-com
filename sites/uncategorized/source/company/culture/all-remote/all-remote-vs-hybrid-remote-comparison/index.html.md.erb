---
layout: handbook-page-toc
title: "オールリモートとハイブリッドの長所の比較リスト"
canonical_path: "/company/culture/all-remote/all-remote-vs-hybrid-remote-comparison/"
description: オールリモートとハイブリッドの長所の比較リスト
twitter_image: "/images/opengraph/all-remote.jpg"
twitter_image_alt: "GitLab remote team graphic"
twitter_site: "@gitlab"
twitter_creator: "@gitlab"
---

## 目次 {#on-this-page}
{:.no_toc}

- TOC
{:toc}

## 概要 {#introduction}

![GitLab all-remote at scale illustration](/images/all-remote/gitlab_all_remote_work_environment_scale.jpg){: .medium.center}

リモートワークにはいろいろな形態があります。ハイブリッドはリモートとオフィスの長所を提供するように見えますが、経営者の多くは、[複雑さと不平等](https://www.wired.com/story/hybrid-remote-work-offers-the-worst-of-both-worlds/)を過小評価しています。オールリモートで働く場合のマネージャーや部下にとっての利点と、ハイブリッドでの利点を比較・検討します。

このページでは、基本リモートで働くオールリモートな職場と、ハイブリッドな職場の違いを明確にします。ハイブリッドな職場における、リモート勤務とオフィス勤務の違いを比較するものではありません。

[求職者](/company/culture/all-remote/jobs/)は[内定を複数](https://carta.com/blog/gitlab-interview-covid-19-remote-landscape/)もらい比較して検討するものです。単一の職場（オールリモート）と複数の職場（ハイブリッド）の運用の[微妙な違い](/company/culture/all-remote/evaluate/)を理解することは、期待値の調整に非常に重要です。オールリモートであれば、どこの会社でも働き方は変わらないと勘違いして、年収とポジションだけで仕事を安易に選択しないようにしてください。仕事選びのプロセスは、[極めて個人的](/company/culture/all-remote/evaluate/)なものです。このガイドの目的は、求職者が[リモートで働くことに大きな価値を見出す](/blog/2019/08/15/all-remote-is-for-everyone/)ことです。逆に、このデータを活用して、ハイブリッド環境では[機能不全](/handbook/values/#five-dysfunctions)と不公平さがありがちなので、その辛さを補うために給与を上げる交渉をすることができます。

## 言語と専門用語 {#language-and-terminology}

この比較検討では、ハイブリッドは、選択肢の広さと柔軟性が制限された環境と定義しています。**ハイブリッドの場合、ある社員がオフィスにいて、別の社員はオフィス外にいることになり、管理する場所が2つになってしまいます。この二箇所にいる社員を管理するのが非常に難しくそもそも不平等です。**

リモートで働かない人は基本的に[オフィスファースト](/company/culture/all-remote/what-not-to-do/)（例：[ホワイトボード](/company/culture/all-remote/collaboration-and-whiteboarding/)を活用して議論する）で仕事をします。リモートで働く人にとっては、そういった仕事上の会話からカヤの外になり昇進の機会を逃します。

この話題についてより深く知りたい方は、以下のガイドをご覧ください。
1. [リモート度を評価](/company/culture/all-remote/evaluate/)
1. [リモートワークの段階](/company/culture/all-remote/stages/)
1. [ハイブリッドリモート：ニュアンスと落とし穴の理解](/company/culture/all-remote/hybrid-remote/)
1. [GitLabがフルリモートの社員をオールリモートという用語をあてる理由](/company/culture/all-remote/terminology/)

## オールリモートとハイブリッドリモートの利点のチェックリスト {#all-remote-vs-hybrid-remote-benefits-checklist}

| 利点                                                      |         オールリモート         |                              ハイブリッドリモート |
| :----------------------------------------------------------- | :------------------------: | :-----------------------------------------: |
| [通勤時間なし](/company/culture/all-remote/benefits/)          |            はい             |                                        はい |
| [ノンリニアワークデー](/company/culture/all-remote/non-linear-workday/) | はい<br> 許されている職種なら | いいえ<br>  皆が同じ時間帯に仕事をする必要があります |
| [私的な理由で引っ越し](/company/culture/all-remote/people/)ができるか |            はい             |                                    限定的 |
| 通勤可能な距離を配慮せず[住む場所](https://www.fastcompany.com/90548691/extremely-transparent-and-incredibly-remote-gitlabs-radical-vision-for-the-future-of-work)を決められる |            はい             |                                    限定的 |
| [開放型オフィスで働く精神的ストレス](https://theconversation.com/open-plan-office-noise-increases-stress-and-worsens-mood-weve-measured-the-effects-162843)を回避する |            はい             |                                    場合による |
|経営幹部やシニアリーダーへ[誰もが連絡をする](/company/culture/all-remote/how-to-work-remote-first/)ことができる  |            はい             |                                         いいえ |
| [成果](/handbook/values/#measure-results-not-hours)にもとづいて褒められたり昇進する (キーマンとの関係性より) |            はい             |                                         いいえ |
| 全員に平等にIT/サポートが提供される                       |            はい             |                                         いいえ |
| 全員が[デジタルツールを使って作業、コラボレーション、コミュニケーション](/blog/2019/06/18/day-in-the-life-remote-worker/)をする |            はい             |                                         いいえ |
| サイロ化した対面会議で共有された情報をいちいち追いかけないで済む |            はい             |                                         いいえ |
| 疎外感からの解放 - 対面で仲良くなったり、会議をしたり、集まりすることから仲間外れにされる恐れ |            はい             |                                         いいえ |
| [直接対面で会い関係を構築する](/company/culture/all-remote/in-person/)ために出張する際は個人でなく会社が経費負担する |            はい             |                                    場合による |
| 罪悪感からの解放（例：自分は通勤しないが、同僚は通勤している） |            はい             |                                         いいえ |
| 柔軟な働き方を認めない上司にリモートワークを認めさせる労力が不要 |            はい             |                                         いいえ |
| 同僚から過剰な成果を求められることがない |            はい             |                                         いいえ |
| [マネージャー](/company/culture/all-remote/being-a-great-remote-manager/)から、監視されたり、無視されたり、ハラスメントされる心配がない |            はい             |                                         いいえ |
| コワーキングセンター、[CODI](https://www.codi.com/)(コワーキングスペースを提供するWeworkと民泊のAirbnbを組み合わせたようなサービス)や喫茶店などの第3の場所で働く自由（会社負担で） |            はい             |                                    場合による |
| 参加者の何人かが同じ会議室に集まり1台のカメラだけの[最悪なハイブリッドビデオ会議](/company/culture/all-remote/meetings/#avoid-hybrid-calls)をしないですむ |            はい             |                                         いいえ |
| 公平な[面接](/company/culture/all-remote/interviews/)条件（例：対面で面接する候補者に有利不利な取り扱いがないこと） |            はい             |                                         いいえ |
| 場所に関係なく[学習と能力開発](/company/culture/all-remote/learning-and-development/)が全員へ提供される |            はい             |                                         いいえ |
| 原則出社ができる／出社する意思があるかどうかに関係なくキャリアアップできる |            はい             |                                         いいえ |
| 直接会って話をしたいから飛行機で出社しろと急に求められることがない |            はい             |                                         いいえ |
| [ビジネスオペレーションチーム](/handbook/business-technology/)は、社員全員に良い影響を与えるツールやテクノロジーの導入に全力を注いでいる（一部の人に合わせない） |            はい             |                                         いいえ |
| 対面での勤務を好む上司や部署、リモート勤務に協力的ではない上司や部署に出会わずにすむ |            はい             |                                         いいえ |
| 全社的な[対面での集まり](/company/culture/all-remote/in-person/)を適宜開く (常にオフィスに集まるのではなく) |            はい             |                                         いいえ |
| オフィスにいる同僚と、給湯室で雑談をしたり、集まって雑談したりすることができる。 |             いいえ             |                                        はい |

***[GitLabでのライフ](/company/culture/#life-at-gitlab) と [オフィスから出る](/company/culture/all-remote/out-of-the-office/)レポートでより詳しく知ることができます。このページに共感された方は、GitLab の [キャリア採用情報](/jobs/)をご覧ください。*** 

## リモートワークの職場での体験を評価する {#assessing-the-remote-workplace-experience}
1. [RemoteRated](https://remoterated.com/)は、宣伝文句を排除して、実際に働いている人の経験に焦点を当て、リモート力で組織を評価し、ランク付けするサイトです。
1. [Glassdoor](https://www.glassdoor.com/)は、まだ`リモート体験`でレビューを絞り込み検索することはできません。しかし`COVID-19関連`でソートすることができます。このフィルターで、特定の会社でのリモートワークの体験に関連するレビューを読むことができます。また、会社のレビュー欄を`リモート`で検索することも有効です。Glassdoorにはリモート関連のブログ記事があります。

## <%= partial("company/culture/all-remote/is_this_advice_any_good_remote.erb") %>

## <%= partial("company/culture/all-remote/coursera_gitlab_remote_course.erb") %>

## 学びを共有する {#contribute-your-lessons}

GitLabは、オールリモートが[未来の働き方](/company/culture/all-remote/vision/)であり、リモートを取り入れようとしている他の会社のために道を示す責任を負っていると考えています。
もし、あなたやあなたの会社が他の方に役立つ経験をお持ちなら、[Merge request](https://docs.gitlab.com/ee/user/project/merge_requests/) を作成し、このページへ貢献してみてください。

----

[オールリモート](/company/culture/all-remote/)のホームへ戻る
