---
layout: markdown_page
title: "エンジニアリング・フェロー・シャドウ"
description: "GitLabエンジニア：エンジニアリング・フェローと1週間一緒に働く"
---

# 概要

## はじめに

エンジニアリング・フェロー・シャドウ・プログラム（旧称：エンジニアリング・フェロー・ペアリング）は、GitLabのエンジニアなら誰でもGitLabエンジニアリング・フェローと一緒に仕事ができるようにするプログラムです。CEOとエンジニアリング・フェローの日常業務の違いを考慮しいくつかの例外はありますが、[CEOシャドープログラム](/ceo/shadow)をモデルにしています。このプログラムでは、遠隔地と対面でのローテーションの両方をサポートしています。参加者は1週間、エンジニアフェローと一緒に仕事をします。

## 目標

このプログラムの目的は、GitLab のエンジニアとその対象者に、エンジニアリング・フェローの日常業務をお手伝いしながら、その全体像や本質を理解してもらうことです。エンジニアは、エンジニアリング・フェローが製品や組織についてどのように考え、どのように見ているのか、また、彼らが会社全体でどのように活動しているのかを学ぶことができます。特にエンジニアリング部門のシニアテクニカルリーダーにとって有益であり、エンジニアリング・フェローは人間関係を構築・強化することができます。

### 何が違うのか

エンジニアリング・フェロー・シャドウ・プログラムは、業績評価や昇進のためのものではありません。エンジニアリング・フェローシャドウに参加することは、昇進や昇給のために必要ではなく、また、多様な応募者がいるため、昇進や昇給の検討材料にすべきではありません。

## プログラムに参加する

### 知っておくべきこと

* エンジニアリング・フェロー・シャドウプログラムは、COVID-19 [渡航制限](https://about.gitlab.com/handbook/ceo/shadow/#travel-guidance-covid-19)のため、一時的に完全リモートとなります。シャドウは、普段の職場環境から全ての会議に参加します。
* 期間は一人あたり1週間です。
* 普段の仕事は一切しないつもりでいてください。チームには有給休暇中であることを伝えてください。
* 対面式に戻る際の旅費・ホテル代は会社が負担します。
* ローテーションは週単位ですが、毎週行われるわけではありません。
* エンジニアリング・フェローは、米国太平洋時間および中央ヨーロッパ時間帯に居住しています。理想的には、あなたのタイムゾーンに近いフェローとのローテーションを選択することです。

### 参加資格

GitLab の [シニア以上](/handbook/engineering/career-development/#engineering) のエンジニアは、エンジニアリング・フェロー・シャドウ・プログラムに応募することが可能です。

### 応募方法

1. [ローテーションスケジュール](#rotation-schedule)に自分を追加するためのマージリクエストを作成する。
1. 上司にマージリクエストを承認してもらう（ただし、マージはしない）。
1. マージリクエストを希望するエンジニアフェローに割り当てる。

質問がある場合は、Slackの[#ef-shadow](https://gitlab.slack.com/archives/C0342KZSUER)チャンネルに参加・投稿してください。

### ローテーションスケジュール {#rotation-schedule}

(表は日付の新しいものから逆順になっています）

| 開始日|終了日|誰|技術フェロー|場所|結果|
|---|---|---|---|---|--- |
| 2020-04-13 | 2020-04-16 | [Illya Klymov](https://gitlab.com/xanf) | @dzaporozhets | REMOTE | [Document](https://docs.google.com/document/d/1oysQJNX_hms8Fq6rCmLT9ob7UTHd3RWOJ9ldwcrCnCU/edit) |
| 2020-03-09 | 2020-03-13 | [Natalia Tepluhina](https://gitlab.com/ntepluhina) | @dzaporozhets | Kharkiv, Ukraine | [Document](https://docs.google.com/document/d/1LjwDjRFfYgCOIu_VgZoonoU3EtM1_zGfVDGhx4pcWiU/edit?usp=sharing)
| 2020-02-24 | 2020-02-28 | [Christian Couder](https://gitlab.com/chriscool) |  @dzaporozhets | Kharkiv, Ukraine | [Document](https://docs.google.com/document/d/1b-z1awjdqkh7s_cvdMbuXxd-U8jwdGYyGxSfhCTLDwM/edit?usp=sharing) |
| 2020-02-03 | 2020-02-07 | [Jason Plum](https://gitlab.com/warheadsse) | @dzaporozhets | Kharkiv, Ukraine | [Document](https://docs.google.com/document/d/1yR0oGiPXsQYUNlW4F5SKy8co1oOwCSBzHiMsuBovRF4/edit) |
| 2020-01-27 | 2020-01-31 | [Imre Farkas](https://gitlab.com/ifarkas) | @dzaporozhets | Kharkiv, Ukraine | [Document](https://docs.google.com/document/d/1LGtf6W1ESzr8U_DDzgvh7AoFfu3yBVTWVo9gNQcvx10/edit) |
| 2019-12-09 | 2019-12-13 | [Sam Beckham](https://gitlab.com/samdbeckham)  | @dzaporozhets | Limassol, Cyprus | [Video Interview](https://www.youtube.com/watch?v=lVKqsB2gePU&feature=youtu.be), [Document](https://docs.google.com/document/d/1CnlwGIGvlzXmLq4Fy1iLNvFedEC1EQI2y0t_AARDfMU/edit) |
| 2019-11-04 | 2019-11-08 | [Tetiana Chupryna ](https://gitlab.com/brytannia) | @dzaporozhets | Kharkiv, Ukraine | [Document](https://docs.google.com/document/d/1crPEyjISN03zqV0HTjCCjI419SNGRH1sU0xxkglGHFM/edit?usp=sharing) |

