---
layout: handbook-page-toc
title: "GitLab オールリモート化ガイド"
canonical_path: "/company/culture/all-remote/guide/"
description: リモートで仕事をする方法 - GitLabガイド
twitter_image: "/images/opengraph/all-remote.jpg"
twitter_image_alt: "GitLab リモートチームグラフィックス"
twitter_site: "@gitlab"
twitter_creator: "@gitlab"
---

## 目次 {#on-this-page}
{:.no_toc}

- TOC
{:toc}

GitLabは、世界65カ国以上に1,500人以上の[チームメンバー](/company/team/)を擁する世界最大級のオールリモートカンパニーです。

当社のオールリモートワーク導入は、[組織設計ジャーナル](https://jorgdesign.springeropen.com/articles/10.1186/s41469-020-00087-8)で紹介されています。また、[ハーバード・ビジネス・スクール](https://www.hbs.edu/faculty/Pages/item.aspx?num=57917)や[INSEAD](https://publishing.insead.edu/case/gitlab)が執筆したケーススタディでも中心的な存在となっています。

このページと下層ページでは、「オールリモート」の本当の意味、[GitLab での仕組み](/company/culture/all-remote/tips/#how-it-works-at-gitlab)、リモートチームのための[ヒントとコツ](/company/culture/all-remote/tips/#tips-for-leaders-and-other-companies)、さらに学ぶための[リソース](/company/culture/all-remote/resources/)などについてお伝えしていきます。

## リモート宣言 {#the-remote-manifesto}

オールリモートワークの推進:

1. 世界各地から（一極集中ではなく）採用されて働ことができる
1. フレキシブルな労働時間（固定された労働時間ではなく）
1. 知識を書き留めること（口頭での説明ではなく）
1. 文書化されたプロセス（OJTではなく）
1. 情報は公開する（必要最低限のアクセス権限ではなく）
1. 誰でも編集できるように文書を開放する（文書のトップダウン管理ではなく）
1. 非同期コミュニケーション(同期コミュニケーションではなく)
1. 仕事は結果で（努力した時間ではなく）
1. フォーマルなコミュニケーション・チャンネル（インフォーマルなコミュニケーション・チャンネルではなく）

## なぜリモートなのか？ {#why-remote}

<!-- blank line -->
<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/GKMUs7WXm-E" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
<!-- blank line -->

>  **"Remote is not a challenge to overcome. It's a clear business advantage."** -Victor, Product Manager, GitLab

From the cost savings on office space to more flexibility in employees' daily lives, all-remote work offers a number of advantages to organizations and their people.

But we also recognize that being part of an all-remote company isn't for everyone. Here's a look at some of the advantages and disadvantages.

## リモートワークの長期的なビジョン {#our-long-term-vision-for-remote-work}

Learn more about GitLab's [long-term vision for remote work](/company/culture/all-remote/vision/), and why we can embrace the future of work right now.

## GitLabがフルリモートワークについて「オールリモート(all-remote)」という言葉をつかう理由 {#why-gitlab-uses-the-term-all-remote-to-describe-its-100-remote-workforce}

All-remote means that each individual in an organization is empowered to work and live where they are most fulfilled. By including the word "all" in "all-remote," it makes clear that every team member is equal. No one, not even the executive team, meets in-person on a daily basis.

Learn more about [why GitLab uses the term **all-remote** to describe its 100% remote workforce](/company/culture/all-remote/terminology/).

## GitLabのリモートワークレポート {#the-remote-work-report-by-gitlab}

Created to foster collaboration, innovation, and evolution in the future of work, the [2020 Global Remote Work Report](/company/culture/all-remote/remote-work-report/) dissects the state of distributed work and surfaces key motivators for both employees and employers.

[Download the full report now](http://learn.gitlab.com/all-remote/remote-work-report).

## GitLabがリモートコラボレーションにGitLabを使う理由 {#why-gitlab-uses-gitlab-for-remote-collaboration}

GitLab is a collaboration tool designed to help people work better together whether they are in the same location or spread across multiple time zones. Originally, GitLab let software developers collaborate on writing code and packaging it up into software applications. Today, GitLab has a wide range of capabilities used by people around the globe in all kinds of companies and roles.

You can learn more at GitLab's [remote team solutions page](/company/culture/all-remote/gitlab-for-remote/).

## オールリモートチームの作り方 {#how-we-built-our-all-remote-team}

As GitLab has grown, we've learned a lot about what it takes to build and manage a fully remote team, and want to share this knowledge to help others be successful.

Find out [how GitLab makes it work](/company/culture/all-remote/tips/#how-it-works-at-gitlab).

##　導入する利点と得られる価値 {#advantages-and-benefits}

Operating in an all-remote environment provides a multitude of benefits and competitive advantages for employees, employers, and the world.

Learn more about [benefits and advantages to operating in an all-remote environment](/company/culture/all-remote/benefits/).

## デメリット {#disadvantages}

Despite its many [advantages](/company/culture/all-remote/benefits/), all-remote work isn't for everyone. It can have disadvantages for potential employees depending on their lifestyle and work preferences, as well as the organization.

Learn more about [disadvantages to all-remote, along with solutions to these challenges](/company/culture/all-remote/drawbacks/).

## GitLabテスト: より良いリモートへの12ステップ {#the-gitlab-test-12-steps-to-better-remote}

Borrowing format from The Joel Test, this [12 question test](/company/culture/all-remote/the-gitlab-test-remote-work/) helps leaders understand if you're running a great remote operation.

## リモートワークの緊急導入：何をすべきか（そして何から始めるべきか） {#remote-work-emergency-plan-what-to-do-and-where-to-start}

Due to global issues concerning COVID-19 (Coronavirus), many employees and employers are facing a new reality: they're remote, and they’re unsure of when they’ll be able to return to the office.

For leaders who are suddenly managing work-from-home teams, here are [five things you can focus on right now](/company/culture/all-remote/remote-work-emergency-plan/) to maximize stability.

## リモートワーク開始ガイド：在宅勤務に適応する方法 {#remote-work-starter-guide-how-to-adjust-to-work-from-home}

For employees who are grappling with a new remote reality (or forced work-from-home), here are [five tips to implement straight away in your journey to acclimate](/company/culture/all-remote/remote-work-starter-guide/).

## リモート導入のステップ {#the-phases-of-remote-adaptation}

As teams grapple with transitioning from a colocated environment to a remote one, it's common to see differing levels of adaptability.

Learn more about the [phases of remote adaptation](/company/culture/all-remote/phases-of-remote-adaptation/).

## GitLabでは「リモート部門責任者」という役割がリーダシップをとっている {#gitlab-pioneers-the-head-of-remote-role}

By hiring a Head of Remote in 2019, GitLab [triggered a global movement](/company/culture/all-remote/head-of-remote/) of appointing a dedicated leader to evolve a company's remote fluency.

This page details the backstory of the movement's origins, explains what a Head of Remote is, provides job description templates, and open source certifications to enhance your markability as a remote work leader.

## 優れたリモート管理者になるために {#being-a-great-remote-manager}

Many traits found in superb remote managers are also found in managers of colocated teams, though there are nuances to serving, leading, and guiding when managing teams that you do not see in-person each day.

Learn what it takes to be a [優れたリモート管理者になるために](/company/culture/all-remote/being-a-great-remote-manager/).

## 人 {#people}

All-remote organizations tend to attract people who place a high degree of value on autonomy, flexibility, empathy, and mobility. It also presents outsized opportunity for people who must live or prefer to live in rural areas, where well-paying careers in technical industries are few and far between.

Learn more about the [types of people who are adopting a remote lifestyle](/company/culture/all-remote/people/).

## フルフレックス {#non-linear-workday}

How diverse, invigorating, gratifying, and productive could your day be if you threw away the notion that you had to stick to a daily routine?

Learn more about what life can look and feel like when [embracing a non-linear workday](/company/culture/all-remote/non-linear-workday/).

## バリュー {#values}

While all-remote isn't a value itself, it's something we do in order to practice our values.

Learn how a collection of values at GitLab [contributes to a thriving all-remote environment](/company/culture/all-remote/values/).

## 持続可能な文化の構築と強化 {#building-and-reinforcing-a-sustainable-culture}

Culture is best defined not by how a company or team acts when all is well; rather, by the behaviors shown during times of crisis or duress.

Learn more about [creating a thriving, lasting remote culture](/company/culture/all-remote/building-culture/).

## 燃え尽き症候群、孤立、不安との戦い {#combating-burnout-isolation-and-anxiety}

Mental health is an important topic for all companies, and creating a healthy remote workplace is essential to business success.

Learn more about [combating burnout, isolation, and anxiety in the remote workplace](/company/culture/all-remote/mental-health/).

## 仕事 {#jobs}

Job seekers are wise to point their efforts towards companies that are built to support 100% remote. You're able to bypass hours of lobbying for a remote arrangement during the interview process, and you're assured that the tools you need to operate effectively from anywhere will be included from the get-go.

Learn more about [all-remote and remote-first organizations leading the way,  job boards that curate high-quality remote roles, and informal job searching tactics](/company/culture/all-remote/jobs/).

## リモートワークの評価方法 {#how-to-evaluate-a-remote-role}

Not every remote job is created equal. Learn more about [considerations and questions to ask when evaluating a remote role](/company/culture/all-remote/evaluate/).

## リモートワークをはじめる {#getting-started-in-a-remote-role}

Learn more about [considerations and tips for starting a new remote role](https://about.gitlab.com/company/culture/all-remote/getting-started/).

## マネジメント {#management}

Managing an all-remote company is much like managing any other company. It comes down to trust, communication, and company-wide support of shared goals.

Learn more about [what's required to effectively and efficiently manage an all-remote company](/company/culture/all-remote/management/).

## リモートワーク優先のためのポカヨケ {#forcing-functions-to-work-remote-first}

オフィスから完全に撤退してオールリモートにするにしても、ハイブリッドリモートでオフィスとリモートのチームメンバーが同じように働けるようにするにしても、リーダーはリモートファーストの実践を徹底するポカヨケを考えなければなりません。

幹部が組織にリモートを本気で導入にするという明確なメッセージを送るための[戦術的で実行可能なステップ](/company/culture/all-remote/how-to-remote-first/)について詳しく知ることができます。

## リモートでコラボレーションをしたりホワイトボーディングをする {#collaboration-and-whiteboarding-with-remote-teams}

"How do you collaborate and whiteboard in a remote environment?" is a frequently asked question. In a colocated setting, collaboration often happens face-to-face with a whiteboard on hand in a conference room. Working remotely sometimes feels like working on your own, with your own calendar, and your own schedule. With a common goal, strategic planning, and the right collaboration tools, working in a remote environment can be even more productive than working in an office.
* ホワイトボードは「ホワイトボードを使用するアクションまたはプロセス、特に。他の人と協力する手段として。」

Learn more about [collaboration and whiteboarding in remote work environments](/company/culture/all-remote/collaboration-and-whiteboarding/).

## リモートオンボーディング {#remote-onboarding}

Onboarding remotely should focus on three key dimensions: the organizational, the technical, and the social. By using this integrated approach, top companies enable their employees to stay and thrive in their roles. We'll show how you can focus on these three key dimensions of onboarding through an all-remote onboarding process.

Learn more in GitLab's [guide to remote onboarding](/company/culture/all-remote/onboarding/).

## Scaling {#scaling}

GitLabは、オールリモートが仕事の未来であり、組織の成長フェーズでうまく機能するだけでなく、時代遅れのオフィスワークモデルよりも*より*組織の成長につながる信じています。

[組織の成長に関するオールリモートの課題、ソリューション、提供する価値](/company/culture/all-remote/scaling/)について学ぶ。

## リモートワークの段階 {#stages-of-remote-work}

Learn more about the [various stages of remote work](/company/culture/all-remote/stages/), from no remote to all-remote.

## リモートワークのヒント {#tips-for-working-remotely}

Building a remote team or starting your first all-remote job? Check out our [tips for working remotely.](/company/culture/all-remote/tips/)

## 資料集 {#resources}

Browse our [resources page](/company/culture/all-remote/resources/) to learn more about GitLab's approach, read about remote work in the news, and see what other companies are leading the way.

We've also compiled a [list of companies](/handbook/got-inspired/) that have been inspired by GitLab's culture.

## 採用 {#hiring}

GitLab envisions a world where talented, driven individuals can find roles and seek employment based on business needs, rather than an oftentimes arbitrary location.

Hiring across the globe isn't without its challenges. There are local regulations and risks unique to countries and regions around the globe. We believe that these challenges are worth overcoming, and opening our talent acquisition pipeline beyond the usual job centers creates a competitive advantage. We hope to see this advantage wane as more all-remote companies are created.

Learn more about [hiring in an all-remote environment](/company/culture/all-remote/hiring/).

## 報酬 {#compensation}

While there are certain complexities to paying team members who are spread out in over 50 countries, we believe that it's worthwhile. Being an all-remote company enables us to [hire the world's best talent](/company/culture/all-remote/hiring/), not just the best talent from a few cities.

Learn more about [compensation in an all-remote environment](/company/culture/all-remote/compensation/).

## 学習と能力開発 {#learning-and-development}

We believe that all-remote companies are at a competitive advantage when it comes to educating and developing team members.

Learn more on how to make [learning and development a companywide mindset in an all-remote environment](/company/culture/all-remote/learning-and-development/).

## 自走 {#self-service}

It's not what you know. It's knowing **where to look**. This is true at GitLab and other organizations that are intentional about documenting processes, and it is entirely counter to how typical work environments are structured.

Learn more about the [importance of self-searching, self-learning, and self-service in an all-remote organization](/company/culture/all-remote/self-service/).

## インフォーマルコミュニケーション {#informal-communication}

In an all-remote environment, informal communication should be formally addressed. Leaders should organize informal communication, and to whatever degree possible, design an atmosphere where team members all over the globe feel comfortable reaching out to anyone to converse about topics unrelated to work.

Learn more about [enabling informal communication in an all-remote company](/company/culture/all-remote/informal-communication/).

## 非同期コミュニケーション {#asynchronous-communication}

In an all-remote setting, where team members are empowered to live and work where they're most fulfilled, mastering asynchronous workflows is vital to avoiding dysfunction and enjoying outsized efficiencies. Increasingly, operating asynchronously is necessary even in colocated companies which have team members on various floors or offices, especially when multiple time zones are involved.

Learn more about [implementing asynchronous workflows](/company/culture/all-remote/asynchronous/) in your organization, and the benefits to both employee and employer.

## ハンドブック優先のドキュメント {#handbook-first-documentation}

A handbook-first organization is home to team members who benefit from having a single source of truth to lean on. This type of organization is able to operate with almost supernatural efficiency. An organization that does not put concerted effort into documenting has no choice but to watch its team members ask and re-ask for same bits of data in perpetuity, creating a torturous loop of interruptions, meetings, and suboptimal knowledge transfers.

Learn more about the [significance of handbook-first documentation](/company/culture/all-remote/handbook-first-documentation/).

## ミーティング {#meetings}

Learn how to decide when a meeting is necessary and [how to optimize them in an all-remote environment](/company/culture/all-remote/meetings/).

## テキストでの効果的で責任あるコミュニケーション {#communicating-effectively-and-responsibly-through-text}

Embracing text communication and learning to use it effectively requires a mental shift. This can feel unusual or even uncomfortable for those who come from a colocated environment, where in-person meetings and vocalized communiques are the norm.

Learn more about [mastering the use of the written word in an all-remote setting](https://about.gitlab.com/company/culture/all-remote/effective-communication/).

## 対面でのコミュニケーション {#in-person-interactions}

While there are tremendous advantages to operating a 100% remote company, leaders should consider being intentional about planning in-person elements, even if they're optional for team members.

Learn more about [considerations for in-person interactions in a remote company](/company/culture/all-remote/in-person/).

## ワークスペース {#workspace}

All-remote enables the creation of a custom office, perfectly tailored for your desires and ergonomic needs.

Learn more about [key considerations when building and evolving your remote workspace](/company/culture/all-remote/workspace/).

## リモートワーカーとしての子育て {#parenting-as-a-remote-worker}

Parents who work from home have unique demands and expectations, as well as unique challenges. Being an excellent coworker and an excellent parent is a tall order, particularly when childcare and school are impacted by a global crisis.

Learn more about [balancing remote work and parenting](/company/culture/all-remote/parenting/).

## 体験談 {#stories}

Read the [stories](/company/culture/all-remote/stories/) of some of our team members and hear how remote work has impacted their lives.

## Inインタビューterviews {#interviews}

Read and listen to [interviews](/company/culture/all-remote/interviews/) on the topic of working remotely, hosted by GitLab team members.

## 沿革 {#history}

Learn about [historical milestones, inflection points, and prescient interviews](/company/culture/all-remote/history/) in the evolution and expansion of remote work.

## ハイブリッド・リモート {#hybrid-remote}

Hybrid-remote companies have one or more offices where a subset of the company commutes to each day, paired with a subset of the company that works remotely.

Learn more about [advantages, challenges, and the primary differences between all-remote and other forms of remote working](/company/culture/all-remote/part-remote/).

## オールリモートとハイブリッドリモートの利点の比較 {#comparing-all-remote-benefits-vs-hybrid-remote-benefits}

Learn more about the [differences in workplace experience](/company/culture/all-remote/all-remote-vs-hybrid-remote-comparison/) by individuals who are *remote by default* in an all-remote setting vs. a hybrid-remote setting, working with colleagues who are onsite by default. 

## リモートへの移行・追加 {#transitioning-or-adding-remote}

Learn more about [considerations for transitioning a colocated or hybrid-remote company to 100% remote](/company/culture/all-remote/transition/).

## リモート導入の際にやってはいけないこと {#what-not-to-do-when-implementing-remote}

An organization should not attempt to merely replicate the in-office/colocated experience, remotely.

Learn more about [what not to do when transitioning to remote, or moving towards remote](/company/culture/all-remote/what-not-to-do/).

## 旅先での仕事 {#working-while-traveling}

Working remotely enables a tremendous amount of freedom, enabling team members to work from anywhere so long as there is a reliable internet connection.

Learn more about [optimizing comfort and efficiency when taking your office with you while traveling](/company/culture/all-remote/working-while-traveling/).

## リモートワークの会議、サミット、イベント {#remote-work-conferences-summits-and-events}

Particularly for those who are seeking a new role with an all-remote or remote-first company, events can be a great place to meet others who have experience and connections in the space.

Learn more about [remote work conferences and summits, the power of networking, and the unique elements of experiencing a virtual event](/company/culture/all-remote/events/).

## リモートインターンシップ {#remote-internships}

Remote internships  are unique in one primary way: there is no physical office involved.

Learn more about considerations for both employee and employer as it relates to [remote internships](/company/culture/all-remote/internship/) (also referred to as apprenticeships and co-ops).

## オールリモート認定 {#all-remote-certification}

GitLab is a pioneer in the all-remote space. As one of the world's largest all-remote companies, we have developed a [custom certification program](/company/culture/all-remote/remote-certification/) to test and apply knowledge attained throughout the all-remote section of the handbook. Emerging leaders must quickly learn and deploy remote-first practices and skills while expanding their knowledge of remote management and overall remote fluency.

## チーム向けリモートワークガイド {#remote-work-guides-for-teams}

These remote work guides are written specifically for external parties who wish to learn how specific divisions/departments function in a remote environment.

### [財務チーム向けリモートガイド](/company/culture/all-remote/guide/finance/) {#[remote-guide-for-finance-teamscompanycultureall-remoteguidefinance}

### [設計チーム向けリモートガイド](/company/culture/all-remote/guide/design/) {#[remote-guide-for-design-teamscompanycultureall-remoteguidedesign}

### [人事のためのリモートガイド](/company/culture/all-remote/guide/people-operations/) {#[remote-guide-for-people-operationscompanycultureall-remoteguidepeople-operations}

### [法務チーム向けリモートガイド](/company/culture/all-remote/guide/legal/) {#[remote-guide-for-legal-teamscompanycultureall-remoteguidelegal}

## オールリモートの「アイディアをお貸しください」インタビュー {#all-remote-pick-your-brain-interviews}

If people want advice on structuring or managing an all-remote organization, we'd love to connect. Learn more about [requesting a Pick Your Brain interview on all-remote](/company/culture/all-remote/pick-your-brain/).

## このページへの貢献方法 {#contribute-to-this-page}

At GitLab, we recognize that the whole idea of all-remote organizations is still
quite new, and can only be successful with active participation from the whole community.
Here's how you can participate:

- Propose or suggest any change to this site by a [merge request](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/).
- [Create an issue](https://gitlab.com/gitlab-com/www-gitlab-com/issues/) if you have any questions or if you see an inconsistency.
- Help spread the word about all-remote organizations by sharing it on social media.
