---
layout: handbook-page-toc
title: 直接の責任者 - DRI - Directly Responsible Individuals
description: "GitLabのDirectly Responsible Individuals（DRI）は、個々のプロジェクト、取り組み、活動のオーナーです。"
---

## 目次 {#on-this-page}
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## DRI(直接責任者:Directive Responsible Individualとは何ですか？)とは何ですか？ {#what-is-a-directly-responsible-individual}

「DRI(直接責任者:Directive Responsible Individual)」という[言葉をAppleは作り](http://fortune.com/2011/08/25/how-apple-works-inside-the-worlds-biggest-startup/)、個々のプロジェクトの責任を負うべき一人の人物を定めました。すべてのプロジェクトにDRIが割り当てられ、プロジェクトの成功（または失敗）に対して最終的な責任を負うというものです。

担当するプロジェクトに取り組むのは、その人だけはありませんが、[「DRI次第で、やり遂げることも、必要なリソースを見つけることもできる」](https://originalfuzz.com/blogs/magazine/83782148-the-directly-responsible-individual)のです。

マネージャーやチームリーダーがDRIに指名されます。あるいはエグゼクティブな時もあります。
またDRIが一人でプロジェクトのすべてを担う場合もあります。
DRIの選抜や具体的な役割は、DRIのスキルセットや割り当てられるタスクの要件に依存します。
最も重要なことは、DRIに権限を与えることです。
[異論を言う、でもコミットする、その後に異論を言う](/handbook/values/#disagree-commit-and-disagree)ことはできますが、決定を変えようとしていたとしても、その決定が正式に覆されるまでは、結果を出すことにコミットすべきです。

DRIは、プロジェクトの成否に対して最終的な責任を負いますが、必ずしもプロジェクトのすべての作業を行う一人で行うわけではありません。DRIは、関係するすべてのチームや利害関係者と**協議し、協力**して、関連するすべての状況を確実に把握し、他の人から意見やフィードバックを集め、関係者間でアクションアイテムやタスクを分担する必要があります。

## DRIの強化 {#empowering-dris}

重要なことは、DRIは決定について誰にも説明する義務がないことです。DRIに説明を強要すると、プロジェクトを水面下で進めるようになります。または説明地獄に陥ることを恐れて、DRIは[責任を負うことを恐れ](/handbook/values/#five-dysfunctions)、[行動を重視](/handbook/values/#bias-for-action)て働くのではなく、何もしないようになるのです。

私たちはむしろDRIが自分の考えをオープンにする文化を育てたいと考えています。そうすれば、幅広い多様な視点のフィードバックが得られます。DRIはそれを考慮に入れながらも、思考を深める方法を選ぶことができます。

<!-- blank line -->
<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/jdN5mj5ieLk?start=1775" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
<!-- blank line -->

ハーバード・ビジネス・スクールのケーススタディー・インタビュー（上図）では、GitLabの共同創業者兼CEOのSid Sijbrandijは、Prithwiraj Choudhury教授と、DRIに関する質問を含め、GitLabのオールリモート構造の様々な要素について話をしました。

> 合議制の組織の良さを引き出すには？何かを決めようとするときは、その旨をみなに伝えます。誰もが意見を述べることができます。
> 
> ヒエラルキー型組織の長所を生かすには、DRI、つまり決定する人を一人置くことです。
> 
> さて、ここで一つ厄介なことがあります。意見を出してくれた人たち全員を納得させたり、説明しすぎたりすると、プロジェクトが水面下で進んでしまうのです。私は、多くの企業でこの現象を見てきました。
> 
> GitLabのDRIは、非常に明確に、なぜその決定をするのかを説明する必要はありませんし、他の人を納得させる必要もありません。
>
> つまり、意見を述べることはできても、最終的な決定に対して意見を聞いたり考慮されたりする権利はないのです。

## DRIと私たちのバリュー {#dris-and-our-values}

**最終的には、[結果](/handbook/values/#results)と[効率](/handbook/values/#efficiency)が重要です。**
DRIは、プロジェクトやチーム内で発生するすべての問題について誰が最終決定権を持つかについて曖昧になる余地がないので、そもそもうまく機能するのです。

**プロジェクトの最終責任者を一人に決めてしまうと、一見、[コラボレーション](/handbook/values/#collaboration)がうまく働かないように思えるかもしれないが、それは誤解です**。
DRIは自分の担当業務に全力で取り組み、成功するために必要なコラボレーションには積極的に取り組むべきです。
DRIにはすべての最終決定を下す権限がありますが、チームや仲間の経験や判断を信頼する方法とタイミングを知っておく必要があります。

**もちろん、物事がうまくいかないとき、責任を取るのは（たいてい）DRIです**。2011年にAppleの悪名高いエラーだらけのマップアプリの再設計について、当時のiOS上級副社長Scott Forestallが、[謝罪の手紙への署名を拒否]( http://fortune.com/2012/10/29/inside-apples-major-shakeup/)して辞任に追い込まれたことがあります。

## プロジェクトDRIの特徴 {#characteristics-of-a-project-dri}

たいがいDRIはタスクレベルで割り当てられます。例えば、新製品の機能を実装する場合、[プロダクトマネージャーは優先順位付けをするDRIであり、エンジニアリングマネージャーはデリバリーのDRIです](/handbook/product/product-processes/#working-with-your-group)。[自分のマネージャーは自分](/handbook/values/#managers-of-one)というバリューのもと、ほとんどのケースではGitLabチームのメンバーは自分が達成すべきタスクのDRIになります。

誰かがプロセスやプロジェクト全体のDRIになることもあります。この場合には大きな責任が伴うため、DRIを割り当てる際に留意すべきことがあります。
[マイク・ブラウン](http://brainzooming.com/about-brainzooming/mike-brown/)は、2015年11月の記事、[プロジェクトマネジメント - DRIの8つの特徴](http://brainzooming.com/project-management-8-chracteristics-of-a-dri/25340/)で、次のような点を挙げています。

1. 戦略的な視点を失うことなく、細部にこだわることができること。
1. 実行と締め切りのプレッシャーの中で冷静な判断ができること。
1. 質問力があり、聞き上手であること。
1. プロジェクト（または戦術やタスク）の方向性をスマートに変化させ、目的に向かって前進し続けることができる。
1. 潜在的な問題を予測し、早期に対処することができる。
1. 組織内の上層部と下層部の間でうまく交流することができる。
1. 挫折から立ち直るための回復力がある。
1. 同じような状況に対して一貫して対応ができる。

DRIはチームの一員です。成功へ歩みを進めるためには、チームはモチベーションを高め足並みを揃える必要があります。
DRIはまた、チームを成功させる責任も担っています。

## コミュニケーションとフィードバック {#communication-and-feedback}

DRIは、目標を明確にし、進捗を確認し、フィードバックを与えたり、フィードバックを受け取る必要があります。
そうすることで、DRIは方向性を変えたり、失敗を避けるために前もって準備をすることができます。

GitLabでは、[非同期](/company/culture/all-remote/management/#asynchronous)でコミュニケーションと仕事をします。それについては、[このページ](/handbook/communication/)で詳しいことを読んでください。

DRIがフィードバックを与えるあるいはフィードバックを受け取る必要があるときに考慮すべきことがあります。
あなたは、同じチームのメンバーでも、実際にマネージャーでもない場合があることです。

フィードバックを与えたり受けたりするのは大変なことです。以前[フィードバック研修のガイダンス](/handbook/people-group/guidance-on-feedback/)でもお話しました。GitLabの[テキストによる効果的で責任のあるコミュニケーションをするためのガイド](/company/culture/all-remote/effective-communication/) も参考にしてください。

## DRI、相談役:Consulted、情報提供者:Informed (DCI) {#dri-consulted-informed-dci}

責任の分担のさせ方は組織によって様々な方法があります。
[RACIマトリクス](https://en.wikipedia.org/wiki/Responsibility_assignment_matrix)が人気があります。
意思決定やプロジェクトにおいて、責任者、説明責任者、相談役、情報提供者が誰になるべきかを示すものです。

GitLabが意思決定にDRIを導入して、RACIマトリックスをDCI（DRI、相談役:Consulted、情報提供者:Informed）へと進化させました。

**DRI**とは、**結果を出す責任**と**説明をする責任**がある人です。

**相談役(Consulted)**は意見を求められる人々です。通常は特定の分野の専門家であり、双方向にコミュニケーションをします。

**情報提供者(Informed)**は、進捗もしくは完了を報告される人のことです。ミッションである[**誰もが貢献できる**](/company/mission/#mission)をふまえると、**情報提供者(Informed)**は、**全員**のことです。

DRIは取り組みに関わっているすべてのチームと**協議**をして、すべての背景や状況が伝わっているか、適切に作業が分担できているかどうか、確認をしてください。

# 承認が必要となる例外的な状況 {#circumstances-requiring-the-rare-need-for-approvals}

調整のために承認が必要だったり、DRI以外の意思決定者がいる状況がありえます。これらは極めてまれなケースです。その必要性を理解しプロジェクトを前へ進めるのがDRIの責任です。

以下のような取り組みでありえます：

- 3つ以上の部門が関与している
- 大きな財務的影響を及ぼす可能性がある
- ビジネスに重大なリスクをもたらす可能性がある
- ビジネス上の評判を考慮する必要がある
- 複数の成功要因を考慮する必要がある（例えば、製品の仕様を変更しiACVを増やしつつ顧客のNPSの維持を両立する）

以下のような例があります:

- 価格設定などの、GitLabと顧客にとって評判や財務に重大な影響を及ぼす部門を横断する大規模な取り組み
- 利用規約の変更などの、複数の部門(例えば、法務、マーケティング、財務など)が共同で対応する必要がある大規模なポリシー変更の施行

このような状況では、DRIとは別な人が最終的な意思決定を担います。だからといって、重要なプロセスを省略したり、他のステークホルダーは関与すべきではない、ということにはなりません。DRIが利害関係者のフィードバックを考慮せず、成功するために必要な計画を立てずに実行し、「Eグループ(経営者)の承認はある」と言っている場合は、一旦立ち止まって大事なプロセスが見落とされていないか、もしくは追加で検討すべき項目がないかどうかを検討するべきです。

決定がなされた後には、DRIは実行して成功させる責任があります。もし、意思決定に同意できないならば、意思決定者の考えを変えるために、説得力のある説明をするのはDRIの責任です。もし合理的な時間内で説得できない場合は、DRIは[異議は伝えつつ、コミットはする](/handbook/values/#transparency-competency)べきです。

## さらに詳しく {#further-reading}

1. [アップルのDRIモデルは実際にどの程度有効なのか](https://www.forbes.com/sites/quora/2012/10/02/how-well-does-apples-directly-responsible-individual-dri-model-work-in-practice/#4d83402d194c)
1. [Matthew Mamet, DRI](https://medium.com/@mmamet/directly-responsible-individuals-f5009f465da4)
1. [テキストによる効果的かつ責任あるコミュニケーション](/company/culture/all-remote/effective-communication/)
1. [GitLabハンドブック, フィードバックに関するガイダンス](/handbook/people-group/guidance-on-feedback/)
